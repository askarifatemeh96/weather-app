import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../../weather.service'

@Component({
  selector: 'app-search-city',
  templateUrl: './search-city.component.html',
  styleUrls: ['./search-city.component.scss']
})
export class SearchCityComponent implements OnInit {

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  cityName: string = 'London';
  showCity(cityName: string) {
    this.cityName = cityName;
  }
}
