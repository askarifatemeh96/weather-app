import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { WeatherService } from '../../../weather.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-card-city',
  templateUrl: './card-city.component.html',
  styleUrls: ['./card-city.component.scss']
})

export class CardCityComponent implements OnInit, OnChanges {

  constructor(private router: Router, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.weatherInfo(this.cityName);
  }

  ngOnChanges(): void {
    this.weatherInfo(this.cityName);
  }

  @Input() cityName: string;
  @Input() addMode: boolean;
  cityInfo: any;
  weatherData: any;
  weatherCondition: string;
  listCity: any;

  weatherInfo(cityName: string) {
    this.weatherService.getWeatherInfo(cityName).subscribe((data: any) => {
      this.weatherData = {
        weatherCondition: data.weather[0].main,
        city: data.name,
        temp: data.main.temp,
        minTemp: data.main.temp_min,
        maxTemp: data.main.temp_max
      }
      this.weatherCondition = data.weather[0].main;
    })
  }

  detail() {
    this.router.navigate(['details', this.weatherData.city]);
  }

  addCityToStorage(city: string) {
    this.listCity = (window.localStorage.getItem("cities")).replace(/[^a-zA-Z +]/g, "");
    var contain = this.listCity.includes(city);

    if (!contain) {
      this.listCity += city + '+';
      alert('successfully added')
    }
    else {
      alert("it's already existed")
    }

    window.localStorage.setItem("cities", JSON.stringify(this.listCity));
  }

  addCity() {
    this.addCityToStorage(this.weatherData.city);
  }

}
