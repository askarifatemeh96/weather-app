import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from 'src/app/weather.service';

import { City } from '../../../../Model/model'

@Component({
  selector: 'app-detail-city',
  templateUrl: './detail-city.component.html',
  styleUrls: ['./detail-city.component.scss']
})
export class DetailCityComponent implements OnInit {

  constructor(private Root: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.getCityName();
  }

  days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  todayNumberInWeek = new Date().getDay();
  today = this.days[this.todayNumberInWeek];
  everyDayTemp: Number[] = [];
  everyDayWeatherCondition: string[] = [];
  cityName: string;
  weatherData: any;
  forecastData: any;
  weatherCondition: any;
  weatherId: string;

  weatherInfo(cityName: string) {
    this.weatherService.getWeatherInfo(cityName).subscribe((data: any) => {
      this.weatherData = {
        weatherCondition: data.weather[0].main,
        city: data.name,
        temp: data.main.temp,
        humidity: data.main.humidity,
        wind: data.wind.speed
      }
      this.weatherCondition = data.weather[0].main;
      this.weatherId = data.id;
      this.forecastInfo(this.weatherData.city);
    })
  }

  getCityName() {
    this.Root.params.subscribe(
      params => {
        this.cityName = params.name;
        this.weatherInfo(this.cityName);
      }
    )
  }

  forecastInfo(id: string) {
    this.weatherService.forecastWeatherInfo(id).subscribe((data: any) => {
      this.forecastData = {
        list: data.list
      }

      for (let i = this.todayNumberInWeek; i < 7; i++) {
        this.everyDayTemp[i] = Math.round(this.forecastData.list[i].main.temp);
        this.everyDayWeatherCondition[i] = this.forecastData.list[i].weather[0].main;
      }

      for (let i = this.todayNumberInWeek - 1; i >= 0; i--) {
        this.everyDayTemp[i] = Math.round(this.forecastData.list[i].main.temp);
        this.everyDayWeatherCondition[i] = this.forecastData.list[i].weather[0].main;
      }
    })
  }
}
