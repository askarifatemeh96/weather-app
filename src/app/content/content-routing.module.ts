import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentComponent } from '../content/content.component';
import { AddCityComponent } from '../content/Components/add-city/add-city.component';
import { CardCityComponent } from '../content/Components/card-city/card-city.component';
import { SearchCityComponent } from '../content/Components/search-city/search-city.component';
import { DetailCityComponent } from '../content/Components/detail-city/detail-city.component';

const routes: Routes = [
  { path: '', component: ContentComponent },
  { path: 'add', component: SearchCityComponent },
  { path: 'details/:name', component: DetailCityComponent },
  { path: '**', component: ContentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ContentRoutingModule { }
