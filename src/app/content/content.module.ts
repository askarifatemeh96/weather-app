import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentRoutingModule } from './content-routing.module';

import { ContentComponent } from '../content/content.component';
import { AddCityComponent } from '../content/Components/add-city/add-city.component';
import { CardCityComponent } from '../content/Components/card-city/card-city.component';
import { SearchCityComponent } from '../content/Components/search-city/search-city.component';
import { DetailCityComponent } from '../content/Components/detail-city/detail-city.component';

import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { TempTransformPipe } from '../temp-transform.pipe';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ContentComponent,
    AddCityComponent,
    CardCityComponent,
    SearchCityComponent,
    DetailCityComponent,
    TempTransformPipe
  ],
  imports: [
    CommonModule,
    ContentRoutingModule,
    MatCardModule,
    MatIconModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class ContentModule { }
