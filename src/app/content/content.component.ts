import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.getArrCity();
  }

  arrCity: any;
  getArrCity() {
    this.arrCity = (window.localStorage.getItem("cities")).replace(/[^a-zA-Z +]/g, "");
    this.arrCity = this.arrCity.split('+');
    this.arrCity.pop();
  }

}
