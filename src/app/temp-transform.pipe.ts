import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tempTransform'
})
export class TempTransformPipe implements PipeTransform {

  transform(value: any): any {
    return Math.round((parseInt(value) - 273.15));
  }

}
