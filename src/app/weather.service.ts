import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private URL = 'https://api.openweathermap.org/data/2.5/weather?q=';
  private forecastURL = 'http://api.openweathermap.org/data/2.5/forecast/?q=';
  private appid = 'bb78f6c6307a1c4d4238561625b33488'
  constructor(private http: HttpClient) {
  }

  getWeatherInfo(city: string) {
    return this.http.get(`${this.URL}${city}&appid=${this.appid}`)
  }

  forecastWeatherInfo(city: string) {
    return this.http.get(`${this.forecastURL}${city}&cnt=7&units=metric&appid=${this.appid}`)
  }

}


